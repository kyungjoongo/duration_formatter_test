function secondsToYMDhms(seconds) {

    if (seconds === 0) {
        return "now"
    } else {
        seconds = Number(seconds);

        let mm = Math.floor(seconds / (3600 * 24 * 30));
        let d = Math.floor(seconds % (3600 * 24) / 3600 / 3600);
        let h = Math.floor(seconds % (3600 * 24) / 3600);
        let m = Math.floor(seconds % 3600 / 60);
        let s = Math.floor(seconds % 60);

        let mmDisplay = mm > 0 ? mm + (mm === 1 ? " month and " : " months and ") : "";
        let dDisplay = d > 0 ? d + (d === 1 ? " day and " : " days and ") : "";
        let hDisplay = h > 0 ? h + (h === 1 ? " hour and " : " hours and ") : "";
        let mDisplay = m > 0 ? m + (m === 1 ? " minute and " : " minutes and ") : "";
        let sDisplay = s > 0 ? s + (s === 1 ? " second" : " seconds") : "";
        let yyDisplay = '';
        if (mm >= 12) {
            let _yy = Math.floor(mm / 12)
            yyDisplay = _yy + (_yy === 1 ? " year and " : " years and ")
            let _mm = Math.floor(mm % 12);
            if (Math.floor(_mm % 12) !== 0) {
                mmDisplay = _mm + (_mm === 1 ? " month and " : " months and ")
            } else {
                mmDisplay = '';
            }
        }

        return yyDisplay + mmDisplay + dDisplay + hDisplay + mDisplay + sDisplay;
    }

}

let result = secondsToYMDhms(34214400 + 15 + 2592000)

console.log("result====>", result);

